import { Entity, Column, CreateDateColumn, PrimaryColumn, ManyToOne, JoinColumn  } from "typeorm";
import { v4 as uuid } from "uuid";
import { Categoria } from "./Categoria";

@Entity("videos")
export class Video {
    @PrimaryColumn()
    id: string

    @Column()
    nome: string

    @Column()
    descricao: string

    @Column()
    categoria_id: string

    @ManyToOne(() => Categoria)
    @JoinColumn( {name: "categoria_id"} )
    categoria: Categoria

    @CreateDateColumn()
    created_at: Date

    constructor() {
        if(!this.id) {
            this.id = uuid()
        }
    }
}