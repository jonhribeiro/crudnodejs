import { Request, Response } from "express";
import { UpdateCategoriaService } from "../services/UpdateCategoriaService";

export class UpdateCategoriaController {
    async handle(request: Request, response: Response) {
        const { id } = request.params;
        const{ nome, descricao } = request.body;
        const service = new UpdateCategoriaService();
        const result = await service.execute({ id, nome, descricao });
        if (result instanceof Error) {
            return response.status(400).json(result.message);
        }
        return response.json(result);
    }
}