import { Request, Response } from "express";
import { CreateCategoriaService } from "../services/CreateCategoriaService";

export class CreateCategoriaController {
    async handle(request: Request, response: Response) {
        const { nome, descricao } = request.body;
        const service = new CreateCategoriaService();
        const result = await service.execute({ nome, descricao });
        if (result instanceof Error) {
            return response.status(400).json(result.message);
        }

        return response.json(result);
    }
}