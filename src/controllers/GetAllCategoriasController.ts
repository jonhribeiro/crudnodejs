import { Request, Response } from "express";
import { GetAllCategoriaService } from "../services/GetAllCategoriaService";

export class GetAllCategoriaController {
    async handle(request: Request, response: Response) {
        const service = new GetAllCategoriaService();
        const categorias = await service.execute();
        return response.json(categorias);
    }
}