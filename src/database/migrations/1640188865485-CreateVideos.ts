import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateVideos1640188865485 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'videos',
                columns: [
                    {
                        name: "id",
                        type: "varchar",
                        isPrimary: true,
                        generationStrategy: 'uuid',
                    }, 
                    {
                        name: 'nome',
                        type: "varchar",
                        isUnique: true
                    },
                    {
                        name: 'descricao',
                        type: "varchar"
                    },
                    {
                        name: 'categoria_id',
                        type: "varchar",
                        generationStrategy: 'uuid',
                    },
                    {
                        name: 'created_at',
                        type: "timestamp",
                        default: "now()"
                    }
                ],
                foreignKeys: [
                    {
                        name: "fk_videos_categorias",
                        columnNames: ["categoria_id"],
                        referencedTableName: "categorias",
                        referencedColumnNames: ["id"]
                    }
                ]
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("videos");
    }

}
