import { getRepository } from "typeorm"
import { Categoria } from "../entities/Categoria";
import { Video } from "../entities/Video"

type VideoRequest = {
    nome: string;
    descricao: string;
    categoria_id: string;
}

export class CreateVideoService {
    async execute({ nome, descricao, categoria_id }: VideoRequest): Promise<Error | Video> {
        const repo = getRepository(Video);
        const repoCategoria = getRepository(Categoria);
        if (!(await repoCategoria.findOne(categoria_id))) {
            return new Error("Categoria não existe");
        }
        const video = repo.create({ nome, descricao, categoria_id });
        await repo.save(video);
        return video;
    }
}