import { getRepository } from "typeorm";
import { Categoria } from "../entities/Categoria";

type CategoriaUpdateRequest = {
    id: string;
    nome: string;
    descricao: string;
};

export class UpdateCategoriaService {
    async execute({ id, nome, descricao }: CategoriaUpdateRequest) {
        const repo = getRepository(Categoria);

        const categoria = await repo.findOne(id);
        if (!categoria) {
            return new Error("Categoria não existe");
        }
        categoria.nome = nome ? nome : categoria.nome;
        categoria.descricao = descricao ? descricao : categoria.descricao;
        await repo.save(categoria);
        return categoria;
    }
}