import { getRepository } from "typeorm";
import { Categoria } from "../entities/Categoria";

type CategoriaRequest = {
    nome: string;
    descricao: string;
};

export class CreateCategoriaService {
    async execute({
        nome,
        descricao,
    }: CategoriaRequest ): Promise<Categoria | Error> {
        const repo = getRepository(Categoria);

        if (await repo.findOne({ nome })) {
            return new Error("Categoria ja Cadastrada no Sistema")
        }

        const categoria = repo.create({
            nome,
            descricao,
        });

        await repo.save(categoria);
        return categoria;
    }
}