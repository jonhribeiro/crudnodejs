import { getRepository } from "typeorm";
import { Video } from "../entities/Video";

type VideoUpdateRequest = {
    id: string;
    nome: string;
    descricao: string;
    categoria_id: string;
};

export class UpdateVideoService {
    async execute({ id, nome, descricao, categoria_id }: VideoUpdateRequest) {
        const repo = getRepository(Video);

        const video = await repo.findOne(id);
        if (!video) {
            return new Error("Video não existe");
        }
        video.nome = nome ? nome : video.nome;
        video.descricao = descricao ? descricao : video.descricao;
        video.categoria_id = categoria_id ? categoria_id : video.categoria_id;
        await repo.save(video);
        return video;
    }
}