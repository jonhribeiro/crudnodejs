import { getRepository } from "typeorm";
import { Categoria } from "../entities/Categoria";

export class DeleteCategoriaService {
    async execute(id: string) {
        const repo = getRepository(Categoria);

        if (!(await repo.findOne(id))) {
            return new Error("Categoria não existe!");
        }

        await repo.delete(id);
    }
}