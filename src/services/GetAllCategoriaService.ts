import { getRepository } from "typeorm";
import { Categoria } from "../entities/Categoria";

export class GetAllCategoriaService {
    async execute() {
        const repo = getRepository(Categoria);
        const categorias = await repo.find();
        return categorias;
    }
}