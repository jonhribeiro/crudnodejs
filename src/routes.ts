import { Router } from "express";
import { CreateCategoriaController } from "./controllers/CreateCategoriaController";
import { CreateVideoController } from "./controllers/CreateVideoController";
import { DeleteCategoriacontroller } from "./controllers/DeleteCategoriaController";
import { DeleteVideocontroller } from "./controllers/DeleteVideoController";
import { GetAllCategoriaController } from "./controllers/GetAllCategoriasController";
import { GetAllVideosController } from "./controllers/GetAllVideosController";
import { UpdateCategoriaController } from "./controllers/UpdateCategoriaController";
import { UpdateVideoController } from "./controllers/UpdateVideoController";

const routes = Router();

routes.post("/categoria", new CreateCategoriaController().handle);
routes.get("/categoria", new GetAllCategoriaController().handle);
routes.delete("/categoria/:id", new DeleteCategoriacontroller().handle);
routes.put("/categoria/:id", new UpdateCategoriaController().handle);

routes.post("/video", new CreateVideoController().handle);
routes.get("/video", new GetAllVideosController().handle);
routes.delete("/video/:id", new DeleteVideocontroller().handle);
routes.put("/video/:id", new UpdateVideoController().handle);

export { routes };